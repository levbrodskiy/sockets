package ru.ivarodskiy.web.messagetrnsfer;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080);
             InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream();
             BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
             BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream))) {
            String response = "Shalom";
            bufferedWriter.write(response);
            System.out.println("отправлено серверу: " + response);
            int count = 1;
            while ((response = bufferedReader.readLine()) != null && count < 10) {
                System.out.println("прислал сервер: " + response);
                bufferedWriter.write(response);
                System.out.println("отправлено серверу: " + response);
                outputStream.flush();
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}