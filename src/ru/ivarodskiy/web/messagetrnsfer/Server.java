package ru.ivarodskiy.web.messagetrnsfer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Сервер ждет клиента...");

        try (Socket clientSocket = serverSocket.accept();
             InputStream inputStream = clientSocket.getInputStream();
             OutputStream outputStream = clientSocket.getOutputStream();
             BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
             BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream))) {
            System.out.println("Новое соединение: " + clientSocket.getInetAddress().toString());

            String request;
            while ((request = bufferedReader.readLine()) != null) {
                System.out.println("прислал клиент: " + request);
                bufferedWriter.write(request);
                System.out.println("отправлено клиенту: " + request);
                outputStream.flush();
            }
            System.out.println("клиент отключился");
        }
    }
}
